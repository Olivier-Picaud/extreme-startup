#!/bin/bash

export ROOT_FOLDER=$( pwd )

M2_HOME="${HOME}/.m2"
M2_CACHE="${ROOT_FOLDER}/maven"


echo "Generating symbolic links for caches"

[[ -d "${M2_CACHE}" && ! -d "${M2_HOME}" ]] && ln -s "${M2_CACHE}" "${M2_HOME}"


        mvn -f source-code/pom.xml $1


if [ $? -ne 0 ]
    then
        exit -1
fi

TARGET="source-code/target"

if [ -d "$TARGET" ]; then
    echo "Moving jars into build-output"
    mv source-code/* build-output/
fi
