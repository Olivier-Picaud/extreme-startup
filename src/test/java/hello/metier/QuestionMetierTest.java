package hello.metier;

import metier.QuestionMetier;
import org.junit.Assert;
import org.junit.Test;

public class QuestionMetierTest {

    @Test
    public void testShouldAddToNumber() {

        int result = QuestionMetier.shouldAddToNumber(4, 5);
        Assert.assertEquals(9, result);
    }

}
