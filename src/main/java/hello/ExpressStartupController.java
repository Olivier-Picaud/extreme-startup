package hello;

import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import metier.QuestionMetier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExpressStartupController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/extreme")
    public String greeting(@RequestParam(value="q", required = false) String question) {
        Logger.getLogger("express-question").info(question  );
        // parse q
        return QuestionMetier.parseQuestion(question);
    }
}
