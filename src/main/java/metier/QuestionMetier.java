package metier;

public class QuestionMetier {

    public static int shouldAddToNumber(int a, int b){
        return a+b;
    }


    public static String parseQuestion(String questionInput){
        String response;
        try {
            String[] insert = questionInput.split(" ");
            response = String.valueOf(shouldAddToNumber(Integer.parseInt(insert[2]), Integer.parseInt(insert[4])));
        } catch (Exception e) {
            return "42";
        }
        return response;
    }
}
